import http from "../http-common";

class AuthService {
  login(username, password) {
    return http
      .post("/auth/signin", {
        username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(name, mobile, email, password, avatar) {
    return http.post("/auth/signup", {
      name,
      mobile,
      email,
      password,
      avatar
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();