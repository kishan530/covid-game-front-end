import http from "../http-common";
import authHeader from './auth-header';

class PlayerDataService {
  startGame(data) {
    return http.post("/players/start-game", data,{ headers: authHeader() });
  }
  gameAction(data) {
    return http.post("/players/game-action", data,{ headers: authHeader() });
  }
  getOne(id) {
    return http.get(`/players/${id}`,{ headers: authHeader() });
  }
  getIntialValues() {
    return {duration:60,life:100};
  }
}

export default new PlayerDataService();