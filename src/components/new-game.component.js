import React, { Component } from "react";
import { ProgressBar } from 'react-bootstrap';
import AuthService from "../services/auth.service";
import PlayerService from "../services/player.service";


export default class NewGame extends Component {
  constructor(props) {
    super(props);
    this.startGame = this.startGame.bind(this);
    this.state = {
      successful: false,
      message: "",
      content: "",
      user: undefined,
      duration:undefined,
      playerLife:undefined,
      covidLife:undefined
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    const intialValues = PlayerService.getIntialValues();
    if (user) {
        this.setState({
          content: user.full_name,
          user: user,
          duration:intialValues.duration,
          playerLife:intialValues.life,
          covidLife:intialValues.life
        });
    }
  }



  startGame() {
   
    this.setState({
      message: "",
      successful: false
    });

    var data = {
      duration:this.state.duration,
      player_id: this.state.user.id,
      covidLife:this.state.covidLife,
      playerLife:this.state.playerLife
    };

    PlayerService.startGame(data).then(
      response => {
        console.log(response.data);
        this.setState({
          message: response.data.message,
          successful: true
        });
        this.props.history.push({
          pathname: '/game-home',
          state: response.data.id
        })
        window.location.reload();
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          successful: false,
          message: resMessage
        });
      }
    );
  }

  render() {
    const { user,playerLife,covidLife } = this.state;
    return (
      <div>
         {user ? (
           <div>
      <div className="list row mb-3">
        <div className="col-sm-6 text-center">
            <label>{user.full_name}</label>
            <ProgressBar now={playerLife} label={`${playerLife}%`} variant="success" />
        </div>
        <div className="col-sm-6 text-center">
            <label>Dragon</label>
            <ProgressBar now={covidLife} label={`${covidLife}%`} variant="danger" />
        </div>
      </div>

      <div className="col-md-12">     
           
           <div className="form-group text-center mt-2">
                  
                  <button onClick={this.startGame} className="btn btn-primary btn-block">Start Game</button>
            </div>
         

          {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
      </div>
      </div>



       ) : (
        <div className="container">
        <header className="jumbotron">
        <h3>{this.state.message}</h3>
        </header>
    </div>
       )}
      
      </div>
    );
  }
}