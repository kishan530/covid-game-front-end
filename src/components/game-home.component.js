import React, { Component } from "react";
import { Modal,ProgressBar } from 'react-bootstrap';
import AuthService from "../services/auth.service";
import PlayerService from "../services/player.service";


export default class GameHome extends Component {
  constructor(props) {
    super(props);
    this.gameAction = this.gameAction.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.newGame = this.newGame.bind(this);
    this.goToHome = this.goToHome.bind(this);
    this.countDown = this.countDown.bind(this);
    this.state = {
      successful: false,
      message: "",
      content: "",
      user: undefined,
      show: false,
      duration:undefined,
      winDuration:0,
      playerLife:undefined,
      covidLife:undefined,
      game_id:undefined,
      logs:[],
      winner: undefined,
    };
    this.timer = undefined;
    this.countDownTimer = 0;
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    const intialValues = PlayerService.getIntialValues();
    const { state } = this.props.location;
    const timeOut = intialValues.duration;
    if (user) {
        this.setState({
          content: user.full_name,
          user: user,
          game_id: state,
          duration:intialValues.duration,
          winDuration:0,
          playerLife:intialValues.life,
          covidLife:intialValues.life
        });
    }
    setTimeout(function(){
 
      this.timer = setInterval(()=> this.gameAction('Dragon','attack',user.full_name), 6000);
      this.countDownTimer = setInterval(this.countDown, 1000);
 
    }.bind(this), 1000);

    setTimeout(function(){
 
      this.gameAction(user.full_name,'timeout','Dragon');
 
    }.bind(this), 1000*timeOut);
   
  }
  componentWillUnmount(){
    clearInterval(this.timer);
    clearInterval(this.countDownTimer);
    this.setState({
      successful: false,
      message: "",
      content: "",
      user: undefined,
      show: false,
      duration:undefined,
      playerLife:undefined,
      covidLife:undefined,
      game_id:undefined,
      logs:[]
    });
  }

  countDown() {
    let seconds = this.state.winDuration + 1;
    this.setState({
      winDuration: seconds,
    });
    if (seconds === this.state.duration) { 
      clearInterval(this.countDownTimer);
    }
  }

  handleClose() {
    this.setState({
      show: false
    });
  }

  newGame() {
    this.props.history.push("/new-game");
    window.location.reload();
  }

  goToHome() {
    this.props.history.push("/home");
    window.location.reload();
  }



  gameAction(name,type,attacked) {
   
    this.setState({
      message: "",
      successful: false,
      show: false
    });

    var data = {
      name:name,
      type: type,
      attacked: attacked,
      covidLife:this.state.covidLife,
      playerLife:this.state.playerLife,
      winDuration:this.state.winDuration,
      game_id:this.state.game_id,
    };
    PlayerService.gameAction(data).then(
      response => {
        var show = false;
        if(response.data.playerLife<=0 || response.data.covidLife<=0){
          clearInterval(this.timer);
          show = true;
        }else{
          if(response.data.winner){
            clearInterval(this.timer);
            show = true;
          }
        } 
        this.setState({
          message: response.data.message,
          logs: response.data.logs.reverse(),
          covidLife:response.data.covidLife,
          playerLife:response.data.playerLife,
          winner:response.data.winner,
          successful: true,
          show: show
        });
        
       
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          successful: false,
          message: resMessage
        });
      }
    );
  }

  render() {
    const { user,show,playerLife,covidLife,logs,winner } = this.state;
    return (
      <div>
         {user ? (
           <div>
       <div className="row">
        <div className="col-sm-9">
      <div className="list row mb-5">
        <div className="col-sm-6 text-center">
            <label>{user.full_name}</label>
            <ProgressBar now={playerLife} label={`${playerLife}%`} variant="success" />
        </div>
        <div className="col-sm-6 text-center">
            <label>Dragon</label>
            <ProgressBar now={covidLife} label={`${covidLife}%`} variant="danger" />
        </div>
      </div>

      <div className="col-md-12">     
           
           <div className="form-group text-center mt-2">
                  
                  <button onClick={() => this.gameAction(user.full_name,'attack','dragon')} className="btn btn-primary btn-block m-1">Attack</button>

                  <button onClick={() => this.gameAction(user.full_name,'blast','dragon')} className="btn btn-primary btn-block m-1">Blast</button>

                  <button onClick={() => this.gameAction(user.full_name,'heal','dragon')} className="btn btn-primary btn-block m-1">Heal</button>

                  <button onClick={() => this.gameAction(user.full_name,'giveUp','dragon')} className="btn btn-primary btn-block m-1">Give Up</button>
            </div>
         

          {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
      </div>
      </div>
      <div className="col-sm-3">
                 <h5>Commentory Box</h5>
                 <div className="commentory-box">
                   <ul>
                 {logs &&
              logs.map((log, index) => (
                <li key={log._id}>{log.text}</li>
              ))}
              </ul>
                  </div>
      </div>
      </div>

      <Modal show={show} onHide={this.goToHome}>
      
        <Modal.Body>
          <div className="text-center">
          {user.full_name===winner ? (
                <h3>You Win!! play again?</h3>
                ) : (
                  <h3>{winner} Win!! play again?</h3>
            )}
          </div>
        </Modal.Body>
        <Modal.Footer className="text-center mb-3">
          <button onClick={this.newGame} className="btn btn-primary btn-block m-1">Yes</button>

          <button onClick={this.goToHome} className="btn btn-secondary btn-block m-1">No</button>
        </Modal.Footer>
        </Modal>
      </div>



       ) : (
        <div className="container">
        <header className="jumbotron">
        <h3>{this.state.message}</h3>
        </header>
    </div>
       )}
      
      </div>
    );
  }
}