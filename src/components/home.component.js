import React, { Component } from "react";

import PlayerService from "../services/player.service";
import AuthService from "../services/auth.service";
import { Link } from "react-router-dom";
import Moment from 'moment';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      user: undefined,
      games:[]
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    var userId = undefined;
    if (user) {
        userId = user.id;
    }
    PlayerService.getOne(userId).then(
      response => {
        this.setState({
          content: response.data.full_name,
          user: response.data,
          games: response.data.games.reverse(),
        });
      },
      error => {
        this.setState({
          content:
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    const { user,games } = this.state;
    return (
        <div>
        {user ? (
      <div className="container">
        <header className="jumbotron">
          <h3>Hello {user.full_name},</h3>
        </header>
        <div className="list row mb-3">
        <div className="col-md-9">
          <h4>Game History</h4>
         </div>
         <div className="col-md-3 text-end">
          <Link to={"/new-game"} className="btn btn-success pull-right">
              New Game
         </Link>
         </div>
         </div>
        <div className="list row">
        <div className="col-md-12">
         
          <table className="table table-striped" border="1">
          <thead>
            <tr>
              <th>Date</th>
              <th>Duration</th>
              <th>Winner</th>
              <th>Winning Duration</th>
            </tr>
            </thead>
            <tbody>
            {games &&
              games.map((game, index) => (
                <tr key={game._id}>
                    <td> {Moment(game.date).format('DD-MM-YYYY')} </td>
                    <td> {game.duration}</td>
                    <td> {game.winner}</td>
                    <td> {game.winDuration}</td>
                </tr>
              ))}
              </tbody>
          </table>

        </div>
        </div>
      </div>
      ) : (
        <div className="container">
            <header className="jumbotron">
            <h3>{this.state.content}</h3>
            </header>
        </div>
        )}
    </div>
    );
  }
}