import axios from "axios";

export default axios.create({
  baseURL: "https://arcane-gorge-93323.herokuapp.com/api",
  headers: {
    "Content-type": "application/json"
  }
});